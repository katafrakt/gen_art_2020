class Art::NoProject < MainLayout
  def content
    div do
      text "No project found"
    end
  end
end