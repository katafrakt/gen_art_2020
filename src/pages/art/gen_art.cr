class Art::GenArt < MainLayout
  needs generator : Generators::AbstractGenerator

  def content
    div do
      raw generator.generate
    end
  end
end