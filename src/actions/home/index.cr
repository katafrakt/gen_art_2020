class Home::Index < BrowserAction
  get "/" do
    html Art::GenArt, generator: Generators::Cosmos.new
  end
end
