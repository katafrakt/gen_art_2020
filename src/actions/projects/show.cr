class Projects::Show < BrowserAction
  MAPPING = {
    "cosmos" => Generators::Cosmos
  }

  get "/:project_name" do
    generator = MAPPING[project_name]?

    if generator
      html Art::GenArt, generator: generator.new
    else
      html Art::NoProject
    end
  end
end