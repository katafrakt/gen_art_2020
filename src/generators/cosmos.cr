require "celestine"
require "./cosmos/planet"
require "./abstract_generator"

class Generators::Cosmos
  include Generators::AbstractGenerator
  
  WIDTH = 1000
  HEIGHT = 600
  PALETTES = [
    ["#5F0F40", "#9A031E", "#FB8B24", "#E36414", "#0F4C5C"],
    ["#031d44", "#04395e", "#70a288", "#dab785", "#d5896f"],
    ["#55dde0", "#33658a", "#f6ae2d", "#2f4858", "#f26419"]
  ]
  STAR_COLOURS = ["#CCCCCC", "#EEEEEE", "#AAAAAA"]

  def generate
    opts = generate_opts
    Celestine.draw do |ctx|
      ctx.view_box = {x: 0, y: 0, w: WIDTH, h: HEIGHT}
      draw_background(ctx)
      draw_stars(ctx)
      draw_planets(ctx, 3, 8, opts)
    end
  end

  private def generate_opts
    {
      vwidth: WIDTH,
      vheight: HEIGHT,
      light_source_x: (Random.rand(80) + 10)/100.0,
      light_source_y: (Random.rand(80) + 10)/100.0
  }.to_h
  end

  private def draw_background(ctx)
    ctx.rectangle do |r|
      r.x = 0
      r.y = 0
      r.width = WIDTH
      r.height = HEIGHT
      
      r
    end
  end

  private def draw_stars(ctx)
    (Random.rand(100) + 100).times do
      pos_x = Random.rand(WIDTH)
      pos_y = Random.rand(HEIGHT)
      radius = Random.rand(3) + 1
      ctx.circle do |c|
        c.x = pos_x
        c.y = pos_y
        c.radius = radius
        c.fill = STAR_COLOURS.sample

        c
      end
    end
  end

  private def draw_planets(ctx, min, max, opts)
    colours = PALETTES.sample.shuffle
    (Random.rand(max - min) + min).times do |i|
      x = i % colours.size
      Generators::Cosmos::Planet.new(opts).generate(ctx, colours[x])
    end
  end
end