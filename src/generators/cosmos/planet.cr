module Generators
  class Cosmos
    class Planet
      @pos_x : Int32
      @pos_y : Int32
      @radius : Int32

      def initialize(opts : Hash(Symbol, Int32 | Float64))
        @opts = opts
        @pos_x = Random.rand(opts[:vwidth].as(Int32) - 200) + 100
        @pos_y = Random.rand(opts[:vheight].as(Int32) - 150) + 100
        @radius = Random.rand(40) + 20
      end

      def generate(ctx, color)
        mask = generate_mask(ctx)
        draw_planet(ctx, color, mask)
      end

      private def generate_mask(ctx)
        gradient = ctx.radial_gradient do |gr|
          gr.id = UUID.random.to_s
          gr.radius = 1.2
          gr.x = @opts[:light_source_x]
          gr.y = @opts[:light_source_y]
          gr.stop do |stop|
            stop.color = "white"
            stop.offset = 10
            stop.offset_units = "%"
            stop.opacity = 1 - (Random.rand(10)/100.0)
            stop
          end
          gr.stop do |stop|
            stop.color = "white"
            stop.offset = 95
            stop.offset_units = "%"
            stop.opacity = 0
            stop
          end
          gr
        end

        ctx.mask do |mask|
          mask.id = UUID.random.to_s
          mask.circle do |c|
            c.radius = @radius
            c.set_fill(gradient)
            c.x = @pos_x
            c.y = @pos_y
            c
          end
          mask
        end
      end

      private def draw_planet(ctx, color, mask)
        # a black circle for background
        ctx.circle do |c|
          c.x = @pos_x
          c.y = @pos_y
          c.radius = @radius
          c.fill = "black"
          c
        end

        fltr = ctx.filter do |f|
          f.id = UUID.random.to_s
          f.turbulence do |t|
            t.type = "fractalNoise"
            t.base_freq = Random.rand(5)/10.0 + 0.5
            t.num_octaves = Random.rand(5) + 5
            t.stitch_tiles = "stitch"
            t
          end

          f
        end

        # an actual planet
        ctx.circle do |c|
          c.x = @pos_x
          c.y = @pos_y
          c.radius = @radius
          c.fill = color
          c.set_mask(mask)
          c
        end

        # texture
        mask = ctx.mask do |mask|
          mask.id = UUID.random.to_s
          mask.circle do |c|
            c.radius = @radius
            c.x = @pos_x
            c.y = @pos_y
            c.fill = "white"
            c
          end
          mask
        end

        ctx.circle do |c|
          c.x = @pos_x
          c.y = @pos_y
          c.radius = @radius
          c.set_filter(fltr)
          c.set_mask(mask)
          c.opacity = 0.4
          c
        end
      end
    end
  end
end